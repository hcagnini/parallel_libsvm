__constant sampler_t sampler = CLK_NORMALIZED_COORDS_FALSE | CLK_ADDRESS_CLAMP_TO_EDGE | CLK_FILTER_NEAREST;

#define DIAGONAL 1
#define ONE_VERSUS_ALL 2
#define GET_INSTANCE 4

__kernel void multiply_matrices(
	__global int *mode, 
	__global int *pivot, 
	__read_only image2d_t image, 
	__global float *buffer) {	

	int thread_index = get_global_id(0); 

	if(thread_index < get_image_height(image)) { //TODO maybe can be replaced with % operation. will calculate twice for some values, but won't lost GPU time
		float sum = 0.f;
		int z, width_pixels = get_image_width(image);

		if(*mode == ONE_VERSUS_ALL) { //dot product of an instance with all others
			for(z = 0; z < width_pixels; z++) {
				sum += dot(
					read_imagef(image, sampler, (int2)(z, *pivot)), read_imagef(image, sampler, (int2)(z, thread_index))
				);
			}
		} else if(*mode == DIAGONAL) { //dot product of an instance with itself
			for(z = 0; z < width_pixels; z++) {
				float4 pixel = read_imagef(image, sampler, (int2)(z, thread_index));
				sum += dot(pixel, pixel);
			}
		} else if(*mode == GET_INSTANCE) { //computes nothing; only returns instance pointed by pivot
			int 
				pixel_index = thread_index / 4, //position of pixel that contains required value
				channel_index = thread_index % 4; //channel is either red, green, blue or alpha

			float4 pixel = read_imagef(image, sampler, (int2)(pixel_index, *pivot));
			float values[4] = {pixel.x, pixel.y, pixel.z, pixel.w};
			sum = values[channel_index];
		}

		buffer[thread_index] = sum;
	} //else does nothing, nothing needed (in case it's outside dataset dimension)
}